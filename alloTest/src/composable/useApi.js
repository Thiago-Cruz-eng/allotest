import { api } from 'boot/axios';

export default function useApi(url) {
  const list = async () => {
    try {
      const { data } = await api.get(url);
      return data;
    } catch (err) {
      throw new Error(err);
    }
  };

  const findOne = async (id) => {
    try {
      const { data } = await api.get(`${url}/${id}`);
      return data;
    } catch (err) {
      throw new Error(err);
    }
  };

  const post = async (dataCard) => {
    try {
      const { data } = await api.post(url, dataCard);
      return data;
    } catch (err) {
      throw new Error(err);
    }
  };

  const update = async (dataCard) => {
    try {
      const { data } = await api.put(`${url}/${dataCard.id}`, dataCard);
      return data;
    } catch (err) {
      throw new Error(err);
    }
  };

  const remove = async (id) => {
    try {
      const { data } = await api.delete(`${url}/${id}`);
      return data;
    } catch (err) {
      throw new Error(err);
    }
  };

  return {
    list,
    findOne,
    post,
    update,
    remove,
  };
}
