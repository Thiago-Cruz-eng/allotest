import useApi from 'src/composable/useApi';

export default function cardsService() {
  const {
    list, findOne, post, update, remove,
  } = useApi('cards');

  return {
    list,
    findOne,
    post,
    update,
    remove,
  };
}
