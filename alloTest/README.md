# alloTest (Teste de Front-end Grupo Allo)


## Entrando no projeto
```bash
garanta que tem o json-server (npm install -g json-server) instalado
globalmente em sua maquina
```

## Rodando o Back-end
```bash
abra o diretorio "mockBackend" em um terminal separado
apos digite o comando
json-server --watch db.json
```

## Instalando as dependencias do front
```bash
abra o diretorio "alloTest" em um terminal separado
apos digite o comando
npm install
```

### Rodando a aplicacao
```bash
no mesmo diretorio:
quasar dev
```

### Solucao utilizada
```bash
Foi desenvolvido um CRUD tendo como parte mais crucial a implementacao com as rotas
consumidas pelo axios, entao para tornar isso mais manutenivel, optei por construir um service.
Esse service atua como uma interface, onde eu nao consumo diretamente o arquivo de rotas
mas sim essa 'interface' apenas, tornando tudo muito mais facil em caso de mudancas.
```
